import createSagaWorker from './create-saga-worker';
import createName from './create-name';
var createSagaWokers = function (actions, effects, resCheck) {
    return Object.keys(actions).reduce(function (r, name) {
        var _a;
        var act = actions[name];
        var effect = effects[createName(name, 'effect')];
        return Object.assign(r, (_a = {},
            _a[createName(name, 'saga-worker')] = createSagaWorker(act, effect, resCheck),
            _a));
    }, {});
};
export default createSagaWokers;
//# sourceMappingURL=create-saga-workers.js.map