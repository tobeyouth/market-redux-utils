import { errorLog } from 'market-utils';
import createFetchEffect from "./create-fetch-effect";
var createEffect = function (effect, params) {
    if (!effect) {
        var method = params.method, api = params.api;
        if (!method && !api) {
            errorLog('createEffect', '默认使用 createFetchEffect, 但是没有找到 method 和 api', 'create-effect');
        }
        return createFetchEffect(method, api);
    }
    return effect.call(null, params);
};
export default createEffect;
//# sourceMappingURL=create-effect.js.map