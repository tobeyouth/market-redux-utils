import { Fetch } from 'market-utils';
var createFetchEffect = function (method, api) {
    if (method === void 0) { method = 'get'; }
    return function (params) {
        return Fetch[method](api, params).then(function (res) {
            return res;
        }).catch(function (err) {
            return err;
        });
    };
};
export default createFetchEffect;
//# sourceMappingURL=create-fetch-effect.js.map