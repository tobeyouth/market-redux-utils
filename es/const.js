export var ACTION_METHODS;
(function (ACTION_METHODS) {
    ACTION_METHODS["fetch"] = "get";
    ACTION_METHODS["update"] = "put";
    ACTION_METHODS["save"] = "put";
    ACTION_METHODS["create"] = "post";
    ACTION_METHODS["delete"] = "delete";
    ACTION_METHODS["remove"] = "delete";
})(ACTION_METHODS || (ACTION_METHODS = {}));
//# sourceMappingURL=const.js.map