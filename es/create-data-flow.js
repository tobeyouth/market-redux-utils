import createActions from "./create-actions";
import createEffects from "./create-effects";
import createSagaWorks from "./create-saga-workers";
import createTasks from "./create-tasks";
import createWatcher from "./create-watcher";
import createState from "./create-state";
import createReducer from "./create-reducer";
import { combineReducers, bindActionCreators } from "redux";
import { takeLatest } from "redux-saga/effects";
import { errorLog } from "market-utils";
import configureStore from "./configure-store";
import createName from "./create-name";
import { createCommonReducer } from './helpers/reducer';
export var DataFlowSuppotTypes;
(function (DataFlowSuppotTypes) {
    DataFlowSuppotTypes["list"] = "list";
    DataFlowSuppotTypes["detail"] = "detail";
})(DataFlowSuppotTypes || (DataFlowSuppotTypes = {}));
export var DEFAULT_DATAFLOW_OPTIONS = {
    type: DataFlowSuppotTypes.list,
    autoTasks: true,
    autoStore: true
};
var DataFlow = (function () {
    function DataFlow(scope, actionNames, options) {
        if (options === void 0) { options = DEFAULT_DATAFLOW_OPTIONS; }
        this.__scope__ = scope;
        this.__actionNames__ = actionNames;
        this.__options__ = options;
        this.actions = this.createActions();
        this.effects = this.createEffects();
        this.sagaWorkers = this.createSagaWorkers();
        this.state = this.createState();
        this.watchers = this.createWatchers();
        this.reducer = this.createReducer();
        var autoTasks = options.autoTasks, autoStore = options.autoStore;
        if (autoTasks) {
            this.tasks = this.createTasks();
        }
        if (autoStore) {
            this.store = this.createStore();
        }
        if (this.store) {
            this.dispatch = this.store.dispatch;
        }
        if (this.store) {
            this.actions = bindActionCreators(this.actions, this.dispatch);
        }
    }
    DataFlow.prototype.createActions = function () {
        return createActions(this.__scope__, this.__actionNames__);
    };
    DataFlow.prototype.createEffects = function () {
        var options = this.__options__;
        var params = options ? {
            api: options.api
        } : {};
        var effects = createEffects(this.__scope__, this.__actionNames__, params);
        if (options && options.effects) {
            for (var name_1 in options.effects) {
                effects[name_1] = options.effects[name_1];
            }
        }
        return effects;
    };
    DataFlow.prototype.createSagaWorkers = function () {
        if (!this.actions) {
            errorLog('createSagaWorkers', 'There is no actions in DataFlow instance', 'create-data-flow');
            return null;
        }
        if (!this.effects) {
            errorLog('createSagaWorkers', 'There is no effects in DataFlow instance', 'create-data-flow');
            return null;
        }
        var resCheck = this.__options__.resCheck;
        return createSagaWorks(this.actions, this.effects, resCheck);
    };
    DataFlow.prototype.createWatchers = function (effectFn) {
        var _this = this;
        if (effectFn === void 0) { effectFn = takeLatest; }
        if (!this.sagaWorkers) {
            errorLog('createWatchers', 'There is no sagaWorkers in DataFlow instance', 'create-data-flow');
            return null;
        }
        return Object.keys(this.sagaWorkers).map(function (sagaName) {
            var actionName = sagaName.replace(/SagaWorker$/gi, '');
            var action = _this.actions[actionName];
            var watchAction = action[action.__actionName__];
            var sagaWorker = _this.sagaWorkers[sagaName];
            return createWatcher(watchAction, sagaWorker, effectFn);
        });
    };
    DataFlow.prototype.createState = function () {
        var state = this.__options__ ? this.__options__.state : {};
        var stateName = createName(this.__scope__, 'state');
        return createState(this.__scope__, this.actions, state);
    };
    DataFlow.prototype.createReducer = function () {
        var options = this.__options__;
        var type = options && options.type ? options.type : 'list';
        var reducers = options ? options.reducers : {};
        var _reducers = Object.assign(createCommonReducer(type, this.actions), reducers);
        return createReducer(this.__scope__, this.actions, this.state, _reducers);
    };
    DataFlow.prototype.createTasks = function () {
        if (!this.watchers) {
            errorLog('createTasks', 'There is no watchers in DataFlow instance', 'create-data-flow');
            return null;
        }
        return createTasks(this.watchers);
    };
    DataFlow.prototype.createStore = function () {
        if (!this.watchers) {
            errorLog('createStore', 'There is no watchers in DataFlow instance', 'create-data-flow');
            return null;
        }
        var reducer = combineReducers(this.reducer);
        return configureStore(reducer, this.tasks);
    };
    return DataFlow;
}());
export { DataFlow };
var createDataFlow = function (scope, actionNames, options) {
    var df = new DataFlow(scope, actionNames, options);
    return df;
};
export default createDataFlow;
//# sourceMappingURL=create-data-flow.js.map