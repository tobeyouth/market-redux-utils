import { createStore, applyMiddleware, compose } from 'redux';
import createSageMiddleware from 'redux-saga';
import logger from 'redux-logger';
var creator;
var sagaMiddleware = createSageMiddleware();
if (process && process.env && process.env.NODE_ENV !== 'dev') {
    creator = compose(applyMiddleware(sagaMiddleware))(createStore);
}
else {
    creator = compose(applyMiddleware(sagaMiddleware), applyMiddleware(logger))(createStore);
}
var configureStore = function (reducer, forkedSaga, initState) {
    var store = creator(reducer, initState);
    if (forkedSaga) {
        sagaMiddleware.run(forkedSaga);
    }
    return store;
};
export default configureStore;
//# sourceMappingURL=configure-store.js.map