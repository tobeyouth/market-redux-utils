var createActionName = function (scope, action, status) {
    return [scope, action, status].filter(function (item) {
        return !!item;
    }).map(function (name) {
        return name.toUpperCase();
    }).join('.');
};
export default createActionName;
//# sourceMappingURL=create-action-name.js.map