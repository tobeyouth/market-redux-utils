import { getType } from 'market-utils';
import createAsyncAction from './create-async-action';
import createSyncAction from './create-sync-action';
export default (function (scope, action, proxy) {
    if (getType(proxy) === 'boolean') {
        return createSyncAction(scope, action);
    }
    return createAsyncAction(scope, action, proxy);
});
//# sourceMappingURL=create-action.js.map