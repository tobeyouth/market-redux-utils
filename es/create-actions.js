import { camelCase } from "market-utils";
import createAction from './create-action';
var createActions = function (scope, actionNames) {
    return actionNames.reduce(function (r, actionName) {
        var _a;
        return Object.assign(r, (_a = {},
            _a[camelCase(scope + "-" + actionName)] = createAction(scope, actionName),
            _a));
    }, {});
};
export default createActions;
//# sourceMappingURL=create-actions.js.map