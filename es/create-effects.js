import createEffect from './create-effect';
import createName from './create-name';
import { ACTION_METHODS } from './const';
export function methodHelper(action) {
    return ACTION_METHODS[action] || 'get';
}
var createEffects = function (scope, actions, payload, effect) {
    return actions.reduce(function (r, action) {
        var _a;
        var method = methodHelper(action);
        var _payload = Object.assign({}, payload, { method: method });
        return Object.assign(r, (_a = {},
            _a[createName(scope, action, 'effect')] = createEffect(effect, _payload),
            _a));
    }, {});
};
export default createEffects;
//# sourceMappingURL=create-effects.js.map