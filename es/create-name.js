import { camelCase } from "market-utils";
var createName = function (scope, action, ext) {
    return camelCase([scope, action, ext].filter(function (item) {
        return !!item;
    }).join('-'));
};
export default createName;
//# sourceMappingURL=create-name.js.map