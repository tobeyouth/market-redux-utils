var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { DEFAULT } from 'market-utils';
var createState = function (scope, actions, state) {
    var status = Object.keys(actions).reduce(function (r, actName) {
        var _a;
        var action = actions[actName];
        return Object.assign(r, (_a = {},
            _a[action.__actionName__ + "Status"] = DEFAULT,
            _a));
    }, {});
    var STATE = __assign({ scope: scope, error: undefined, data: null }, status, state);
    return STATE;
};
export default createState;
//# sourceMappingURL=create-state.js.map