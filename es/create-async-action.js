import { createFetchTypes } from 'market-utils';
var createAsyncAction = function (scope, actionName, proxy) {
    var asyncTypes = createFetchTypes(scope, actionName);
    var _action = function (payload) {
        var _payload = payload;
        if (proxy) {
            _payload = proxy.call(this, payload);
        }
        return {
            type: asyncTypes[actionName],
            payload: _payload
        };
    };
    Object.keys(asyncTypes).forEach(function (_type) {
        _action[_type] = asyncTypes[_type];
    });
    _action.__async__ = true;
    _action.__scope__ = scope;
    _action.__actionName__ = actionName;
    return _action;
};
export default createAsyncAction;
//# sourceMappingURL=create-async-action.js.map