var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var createReducer = function (scope, actions, state, reducers) {
    var _a;
    var STATE = __assign({}, state);
    var _reducer = function (state, action) {
        if (state === void 0) { state = STATE; }
        if (!action) {
            return state;
        }
        var type = action.type;
        if (reducers && reducers[type]) {
            return reducers[type](state, action);
        }
        return state;
    };
    return _a = {},
        _a[scope + "State"] = _reducer,
        _a;
};
export default createReducer;
//# sourceMappingURL=create-reducer.js.map