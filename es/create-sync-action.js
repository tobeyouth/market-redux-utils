var createSyncAction = function (scope, action) {
    var _action = function (payload) {
        return {
            type: [scope, action].filter(function (k) {
                return !!k;
            }).join('.').toUpperCase(),
            payload: payload
        };
    };
    _action.__async__ = false;
    _action.__scope__ = scope;
    _action.__actionName__ = action;
    return _action;
};
export default createSyncAction;
//# sourceMappingURL=create-sync-action.js.map