var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { SUCCESS, PENDING, FAIL } from 'market-utils';
import createActionName from './../create-action-name';
export var createCommonReducer = function (type, actions) {
    if (type === 'list') {
        return createListReducers(actions);
    }
    else {
        return createDetailReducers(actions);
    }
};
export function createListReducers(actions) {
    return Object.keys(actions).reduce(function (r, actName) {
        var _a;
        var act = actions[actName];
        var status = act.__actionName__ + "Status";
        var startAction = createActionName(act.__scope__, act.__actionName__);
        return Object.assign(r, (_a = {},
            _a[startAction] = function (state, action) {
                var _state = __assign({}, state);
                _state[status] = PENDING;
                return _state;
            },
            _a[act.success] = function (state, action) {
                var _state = __assign({}, state);
                _state[status] = SUCCESS;
                if (_state.data && Array.isArray(_state.data)) {
                    if (action.query && parseInt(action.query.page, 10) === 1) {
                        _state.data = action.res.data;
                    }
                    else {
                        _state.data = _state.data.concat(action.res.data);
                    }
                }
                else {
                    _state.data = action.res.data;
                }
                if (action.payload && action.payload.page) {
                    _state.query = {
                        page: action.payload.page,
                        page_size: action.payload.page_size
                    };
                }
                return _state;
            },
            _a[act.fail] = function (state, action) {
                var _state = __assign({}, state);
                _state[status] = FAIL;
                return _state;
            },
            _a));
    }, {});
}
export function createDetailReducers(actions) {
    return Object.keys(actions).reduce(function (r, actName) {
        var _a;
        var act = actions[actName];
        var status = act.__actionName__ + "Status";
        var startAction = createActionName(act.__scope__, act.__actionName__);
        return Object.assign(r, (_a = {},
            _a[startAction] = function (state, action) {
                var _state = __assign({}, state);
                _state[status] = PENDING;
                _state.error = '';
                return _state;
            },
            _a[act.success] = function (state, action) {
                var _state = __assign({}, state);
                _state[status] = SUCCESS;
                _state.error = '';
                _state.data = action.res.data;
                return _state;
            },
            _a[act.fail] = function (state, action) {
                var _state = __assign({}, state);
                _state[status] = FAIL;
                _state.error = action.error;
                return _state;
            },
            _a));
    }, {});
}
//# sourceMappingURL=reducer.js.map