import createName from "./create-name";
var createEffectName = function (scope, action) {
    return createName(scope, action, 'effect');
};
export default createEffectName;
//# sourceMappingURL=create-effect-name.js.map