# market-redux-utils

针对 redux + redux-saga 定制的一套方法，包含以下方法


### action
- [create-action](./docs/create-action.md)
- [create-actions](./docs/create-actions.md)**内部方法**
- [create-async-action](./docs/create-async-action.md)
- [create-sync-action](./docs/create-sync-action.md)

### effect
- [create-effect](./docs/create-effect.md)
- [create-fetch-effect](./docs/create-fetch-effect.md)
- [create-effects](./docs/create-effects.md)**内部方法**

### saga
- [create-saga-worker](./docs/create-saga-worker.md)
- [create-saga-workers](./docs/create-saga-workers.md)**内部方法**


### store
- [configure-store](./docs/configure-store.md)

### other
- [creact-action-name](./docs/create-action-name.md)
- [create-effect-name](./docs/create-effect-name.md)

### data flow
- [data-flow](./docs/data-flow.md)


### scripts

- compile: `npm run compile`
- compile:esm: `npm run compile:esm`
- example: `npm run example`

## redux 各个部分命名规则

- action-types: 建议使用`${scope.toUpperCase()}.${actionType.toUpperCase()}` 这样的规则，例如:`ARTICLE.CREATE`, `ARTICLE.CREATE.SUCCESS`
- effect: 建议使用`${scope}${action 首字母大写}Effect` 这样的形式，例如: `articleCreateEffect`
- sagaWorker: 建议使用 `${scope}${action 首字母大写}SagaWorker` 这样的形式，例如：`articleCreateSagaWorker`