import React, { ComponentType, ReactChildren } from 'react';
import ReactDOM from 'react-dom';
import { Provider, connect } from 'react-redux';
import createDataFlow from './../../src/create-data-flow';
import createActionName from './../../src/create-action-name';
import createEffectName from './../../src/create-effect-name';
import { Fetch } from 'market-utils';


const df = createDataFlow('articles', 
  ['create', 'delete', 'update', 'fetch'], 
  {
    api: '/api/articles',
    autoStore: true,
    autoTasks: true,
    state: {
      pages: [],
      query: {
        page: 1,
        page_size: 10
      }
    },
    effects: {
      [createEffectName('articles', 'fetch')] () {
        console.warn(`${createEffectName('articles', 'fetch')} 已经被替换`);
        return Fetch.get('/api/articles', {page:100, page_size: 1})
                    .then((res:any):any => {return res});
      }
    },
    reducers: {
      [createActionName('articles', 'fetch', 'success')] (state:any, action:any):any {
        const _state = { ...state };
        _state.data = _state.data ? _state.data.concat(action.res.data) : action.res.data;
        _state.pages = [..._state.pages, action.payload.page];
        _state.query = {
          page: action.payload.query,
          page_size: action.payload.page_size
        }
        return _state;
      }
    }
  });


interface ArticleProps {
  actions: any,
  articlesState: any
}
interface ArticleState {}

class Articles extends React.Component<ArticleProps, ArticleState> {

  componentDidMount () {
    const { actions, articlesState } = this.props;
    if (articlesState) {
      actions.articlesFetch(articlesState.query);
    }
  }

  public render () {
    const { articlesState } = this.props;
    const { data, pages } = articlesState;

    if (!data) {
      return (
        <div className='empty'>
          啥也没有
        </div>
      )
    }
    
    return (
      <div className='article'>
        <ul className="list">
          {
            data.map((item:any, idx:number) => {
              return (
                <li key={ idx }>
                  <p>id: { item.id }</p>
                  <p>title: { item.title }</p>
                  <p>create titme: { item.createTime }</p>
                </li>
              )
            })
          }
        </ul>
        <div>
          共加载了 { pages.length } 页
        </div>
        <div className="buttons">
          <button onClick={ this.fetchItem.bind(this) }>加载一些数据</button>
        </div>
      </div>
    )
  }

  private fetchItem () {
    const { actions, articlesState } = this.props;
    const { query } = articlesState;
    const _query = {
      ...query,
      page: query.page + 1
    }
    actions.articlesFetch(_query);
  }
  
}

interface AppProps {}
interface AppState {}

class ArticlesApp extends React.Component<AppProps, AppState> {

  public render () {
    const View:ComponentType = connect((state) => { return state }, () => {
      return {
        actions: df.actions
      }
    })(Articles);
    return (
      <Provider store={ df.store }>
        <View { ...this.props } />
      </Provider>
    )
  }
}

ReactDOM.render(<ArticlesApp />, document.getElementById('app'));