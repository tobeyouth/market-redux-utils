const titles = [
  '测试标题', '一个测试', '究竟是什么？', '你到底在做什么?', '写这个代码有用吗？',
  '你的人生充满惊喜吗', '确定是惊喜而不是惊吓吗'
]
// helper
function createList(len=10) {
  return Array.from(new Array(len)).map((un, idx) => {
    let t = titles[parseInt(Math.random() * titles.length)];
    return {
      id: idx,
      title: t,
      createTime: new Date().getTime()
    }
  })
}

const mock = {
  'GET /api/articles': (req, res) => {
    return res.send({r: 0, data: createList(parseInt(Math.random() * 10 + 1))})
  }
}

module.exports = mock;