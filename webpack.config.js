const path = require('path');
const apiMocker = require('webpack-api-mocker')

module.exports = {
  entry: {
    app: './example/articles/app.tsx',
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].js',
    hotUpdateChunkFilename: '.hot/hot-update.js',
    hotUpdateMainFilename: '.hot/hot-update.json'
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'babel-loader'
          },
          {
            loader: 'ts-loader'
          }
        ]
      }
    ]
  },
  mode: 'development',
  cache: true,
  devtool: 'cheap-module-eval-source-map',
  watch: true,
  devServer: {
    contentBase: path.resolve(__dirname, './example/articles'),
    watchContentBase: true,
    watchOptions: {
      poll: true
    },
    port: 9000,
    open: true,
    hot: true,
    before (app) {
      apiMocker(app, path.resolve(__dirname, './example/articles/mock.js'), {
        changeHost: true
      })
    }
  }
}