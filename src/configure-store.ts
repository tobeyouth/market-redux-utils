import { createStore, applyMiddleware, compose, 
  Store, StoreEnhancerStoreCreator, Reducer } from 'redux';
import createSageMiddleware from 'redux-saga';
import logger from 'redux-logger';
import { Tasks } from './create-tasks';

let creator:StoreEnhancerStoreCreator;

const sagaMiddleware = createSageMiddleware();

if (process && process.env && process.env.NODE_ENV !== 'dev') {
  creator = compose(
    applyMiddleware(sagaMiddleware)
  )(createStore);
} else {
  creator = compose(
    applyMiddleware(sagaMiddleware),
    applyMiddleware(logger)
  )(createStore);
}

export interface ConfigureStore {
  (reducer:Reducer, saga:Tasks, initState?:any):Store
}

const configureStore:ConfigureStore = function(reducer, forkedSaga, initState?) {
  const store:Store = creator(reducer, initState);
  if (forkedSaga) {
    sagaMiddleware.run(forkedSaga);
  }
  return store;
}

export default configureStore;