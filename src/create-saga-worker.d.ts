import { FetchTypes } from 'market-utils';
import { ActionFunction } from './const';
export interface ResCheck {
    (res: any): boolean;
}
export interface CreateSagaWorker {
    (action: ActionFunction | FetchTypes, effect: any, resCheck?: ResCheck): any;
}
declare const createSagaWorker: CreateSagaWorker;
export default createSagaWorker;
