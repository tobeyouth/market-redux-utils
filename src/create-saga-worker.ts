/**
 * create saga worker
 */
import { apply, put, call } from 'redux-saga/effects';
import { FetchTypes } from 'market-utils';
import { ActionFunction, Action } from './const';

export interface ResCheck {
  (res:any): boolean
}

export interface CreateSagaWorker {
  (
    action: ActionFunction | FetchTypes,
    effect: any,
    resCheck?: ResCheck
  ): any
}

// helper
const defaultResCheck:ResCheck = (res) => {
  const { r } = res;
  return r === 0 || parseInt(<string>r, 10) === 0;
}

const createSagaWorker:CreateSagaWorker = (action, effect, resCheck=defaultResCheck):any => {
    return function* (actionArgs:Action) {
      const { type, payload } = actionArgs;
      const res:any = yield call(effect, payload);
      if (resCheck(res)) {
        return yield put({ type: action.success, res, payload })
      } else {
        return yield put({ type: action.fail, res, payload });
      }
    }
}

export default createSagaWorker;