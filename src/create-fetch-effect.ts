/**
 * create fetch effects
 */
import { Fetch } from 'market-utils';

export interface EffectFn {
  (method:string, api:string, params?:any): Promise<any>
}

const createFetchEffect = (method='get', api:string) => {
  return function (params?:any):Promise<any> {
    return Fetch[method](api, params).then(<T>(res:any):T => {
      return res
    }).catch(<T>(err:any):T => {
      return err
    })
  }
}

export default createFetchEffect;