/**
 * create action name for data-flow options.reducers key
 * like this:
 * DataFlow({
 *  reducers: {
 *    [createActionName(scope, action, status)] (state) {
 *      return state
 *    }
 *  }
 * })
 */

export interface CreateActionName {
  (scope:string, action:string, status?:string):string
}

const createActionName:CreateActionName = (scope, action, status) => {
  return [scope, action, status].filter((item) => {
    return !!item;
  }).map((name) => {
    return name.toUpperCase();
  }).join('.')
}

export default createActionName;