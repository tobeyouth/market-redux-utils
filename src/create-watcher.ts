import { takeEvery, takeLatest, ForkEffect } from "redux-saga/effects";

/**
 * return redux-saga watcher
 */

export type Watcher = () => IterableIterator<ForkEffect>;

export default (action:string, worker:GeneratorFunction, 
  effectFn=takeLatest):Watcher => {
  return function* () {
    yield effectFn(action, worker);
  }
}