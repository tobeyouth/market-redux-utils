export declare function methodHelper(action: string): string;
export interface Effects {
    [key: string]: Function;
}
declare const createEffects: (scope: string, actions: string[], payload?: any, effect?: Function) => Effects;
export default createEffects;
