/**
 * create new async action with types
 */
import { createFetchTypes } from 'market-utils';
import { ActionFunction, Action } from './const';

export interface AsyncAction {
  (scope: string, actionName: string, proxy?: Function): ActionFunction
}

const createAsyncAction:AsyncAction = function(scope, actionName, proxy) {
  const asyncTypes:any = createFetchTypes(scope, actionName);
  const _action = <ActionFunction>function(payload: any):Action {
    let _payload = payload;

    if (proxy) {
      _payload = proxy.call(this, payload);
    }

    return {
      type: asyncTypes[actionName],
      payload: _payload
    }
  }

  // set types to _action
  Object.keys(asyncTypes).forEach((_type) => {
    _action[_type] = asyncTypes[_type];
  })
  _action.__async__ = true;
  _action.__scope__ = scope;
  _action.__actionName__ = actionName;

  return _action
}

export default createAsyncAction;