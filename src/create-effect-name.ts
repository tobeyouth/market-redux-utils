import createName from "./create-name";

/**
 * create effetc name by scope & action
 */
export interface CreateEffectName {
  (scope:string, action:string): string
}

const createEffectName:CreateEffectName = function(scope, action) {
  return createName(scope, action, 'effect');
}

export default createEffectName;