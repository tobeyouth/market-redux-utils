/**
 * 提供一个标准化的 reducer 创建方法
 * 会自动创建:
 * - 四种状态：fetchStatus, createStatus, updateStatus, deleteStatus
 * - 数据对象: data<array|object>
 * - 错误信息: message<string>
 * - 查询条件: query<object>
 */

import { AnyAction, Reducer } from 'redux';
import { StateAny } from './create-state';

export interface ReducerDict {
  [reduerName: string]: Reducer
}

export interface CreateReducer {
  (scope:string,  actions:any, state:StateAny, reducers?:any): ReducerDict
}

const createReducer:CreateReducer = (scope, actions, state, reducers):ReducerDict => {
  const STATE:StateAny = { ...state };
  const _reducer = function (state=STATE, action:AnyAction) {
    if (!action) {
      return state
    }

    const { type } = action;
    if (reducers && reducers[type]) {
      return reducers[type](state, action);
    }

    return state;
  };
  
  return {
    [`${scope}State`]: _reducer
  }
}

export default createReducer;