/**
 * create action
 */

import { getType } from 'market-utils';
import createAsyncAction from './create-async-action';
import createSyncAction from './create-sync-action';
import { ActionFunction } from './const';

export default (scope:string, action: string, proxy?:boolean | Function):ActionFunction => {
  if (getType(proxy) === 'boolean') {
    return createSyncAction(scope, action);
  }

  return createAsyncAction(scope, action, <Function>proxy);
}