import { ForkEffect } from 'redux-saga/effects';
import { Watcher } from './create-watcher';
export declare type Tasks = () => IterableIterator<Array<ForkEffect>>;
declare const createTasks: (watchers: Watcher[]) => Tasks;
export default createTasks;
