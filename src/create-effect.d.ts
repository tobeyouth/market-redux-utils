declare const createEffect: (effect?: Function, params?: any) => Function;
export default createEffect;
