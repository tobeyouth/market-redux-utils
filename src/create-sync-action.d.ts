import { ActionFunction } from './const';
export interface SyncAction {
    (scope: string, action: string): ActionFunction;
}
declare const createSyncAction: SyncAction;
export default createSyncAction;
