import { Store, Reducer } from 'redux';
import { Tasks } from './create-tasks';
export interface ConfigureStore {
    (reducer: Reducer, saga: Tasks, initState?: any): Store;
}
declare const configureStore: ConfigureStore;
export default configureStore;
