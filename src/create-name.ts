import { camelCase } from "market-utils";

/**
 * create formated name by scope & action
 * like this:
 * createName('article', 'create', 'saga-worker);
 * // => 'articleCreateSagaWorker'
 */

export interface CreateName {
  (scope:string, action:string, ext?:string): string
}

const createName:CreateName = function(scope, action, ext) {
  return camelCase([scope, action, ext].filter((item) => {
    return !!item
  }).join('-'));
}

export default createName;