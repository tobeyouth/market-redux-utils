/**
 * create state with ${action}Status by actions
 */

import { DEFAULT } from 'market-utils';

export interface State {
  scope: string,
  error: any,
  data: any,
  [status: string]: any
}

export interface StateAny {
  query?: any,
  [prop:string]: any
}

export interface CreateState {
  (scope: string, actions: any, state?:any):StateAny
}

const createState:CreateState = (scope, actions, state) => {
  const status:any = Object.keys(actions).reduce((r, actName) => {
    const action = actions[actName];
    return Object.assign(r, {
      [`${action.__actionName__}Status`]: DEFAULT
    })
  }, {});

  const STATE:StateAny = {
    scope,
    error: undefined,
    data: null,
    ...status,
    ...state
  };

  return STATE;
}

export default createState;