/**
 * change status change
 */
import { AnyAction } from 'redux';
import { DEFAULT, SUCCESS, PENDING, FAIL } from 'market-utils';
import { StateAny } from './../create-state';
import createActionName from './../create-action-name';


export interface ReducerHelper {
  (action: AnyAction, state:any, actions:any):StateAny
}

export const createCommonReducer = (type: string, actions:any):any => {
  if (type === 'list') {
    return createListReducers(actions);
  } else {
    return createDetailReducers(actions);
  }
}

/**
 * 默认的 list 格式数据处理
 */
export function createListReducers(actions:any):any {

  return Object.keys(actions).reduce((r, actName) => {
    const act = actions[actName];
    const status:string = `${act.__actionName__}Status`;
    const startAction:string = createActionName(act.__scope__, act.__actionName__);

    return Object.assign(r, {
      [startAction]: (state:any, action:AnyAction):any => {
        const _state = { ...state };
        _state[status] = PENDING;
        return _state;
      },
      [act.success]: (state:any, action:AnyAction):any => {
        const _state = { ...state };
        _state[status] = SUCCESS;
        /**
         * 几种情况
         * 1. state.data 为空时, 默认会把 action.res.data 写入到 state.data 上
         * 2. state.data 有值且类型时数组时，会判断 query
         *   2.1 如果 action.query.page 为 1, 则继续覆盖
         *   2.2 如果 action.query.page 不为 1 或者没有 action.query, 
         *       则会把 action.res.data concat 到 state.data 后面
         * 3. 其他情况，则默认用 action.res.data 重新赋值给 _state.data
         */
        if (_state.data && Array.isArray(_state.data)) {
          // 2.2
          if (action.query && parseInt(action.query.page, 10) === 1) {
            _state.data = action.res.data;
          } else {
            _state.data = _state.data.concat(action.res.data)
          }
        } else {
          _state.data = action.res.data;
        }
        // set query
        if (action.payload && action.payload.page) {
          _state.query = {
            page: action.payload.page,
            page_size: action.payload.page_size
          }
        }
        return _state;
      },
      [act.fail]: (state:any, action:AnyAction):any => {
        const _state = { ...state };
        _state[status] = FAIL;
        return _state;
      }
    })
  }, {});
}

/**
 * 默认的 detail 模式数据处理
 */
export function createDetailReducers(actions:any):any {
  
  return Object.keys(actions).reduce((r, actName) => {
    const act = actions[actName];
    const status:string = `${act.__actionName__}Status`;
    const startAction:string = createActionName(act.__scope__, act.__actionName__);

    return Object.assign(r, {
      [startAction]: (state:any, action:AnyAction):any => {
        const _state = { ...state };
        _state[status] = PENDING;
        _state.error = '';
        return _state;
      },
      [act.success]: (state:any, action:AnyAction):any => {
        const _state = { ...state };
        _state[status] = SUCCESS;
        _state.error = '';
        _state.data = action.res.data;
        return _state;
      },
      [act.fail]: (state:any, action:AnyAction):any => {
        const _state = { ...state };
        _state[status] = FAIL;
        _state.error = action.error;
        return _state;
      }
    })
  }, {});
}