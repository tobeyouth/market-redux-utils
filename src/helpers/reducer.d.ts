import { AnyAction } from 'redux';
import { StateAny } from './../create-state';
export interface ReducerHelper {
    (action: AnyAction, state: any, actions: any): StateAny;
}
export declare const createCommonReducer: (type: string, actions: any) => any;
export declare function createListReducers(actions: any): any;
export declare function createDetailReducers(actions: any): any;
