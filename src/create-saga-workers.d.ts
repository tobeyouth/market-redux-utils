import { ResCheck } from './create-saga-worker';
export interface SagaWorkersDict {
    [sagaWorkerName: string]: GeneratorFunction;
}
export interface CreateSagaWorkers {
    (actions: any, effects: any, resCheck: ResCheck): SagaWorkersDict;
}
declare const createSagaWokers: CreateSagaWorkers;
export default createSagaWokers;
