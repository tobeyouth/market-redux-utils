import { ActionFunction } from "./const";
export interface Actions {
    [key: string]: ActionFunction;
}
export interface CreateActions {
    (scope: string, actionNames: string[]): Actions;
}
declare const createActions: CreateActions;
export default createActions;
