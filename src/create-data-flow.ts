import createActions, { Actions } from "./create-actions";
import createEffects, { Effects } from "./create-effects";
import createSagaWorks, { SagaWorkersDict } from "./create-saga-workers";
import createTasks, { Tasks } from "./create-tasks";
import createWatcher, { Watcher } from "./create-watcher";
import createState, { StateAny } from "./create-state";
import createReducer, { ReducerDict } from "./create-reducer";
import { AnyAction, combineReducers, bindActionCreators, Dispatch, Store } from "redux";
import { takeLatest } from "redux-saga/effects";
import { errorLog } from "market-utils"
import configureStore from "./configure-store";
import createName from "./create-name";
import { createCommonReducer } from './helpers/reducer';
import { ResCheck } from './create-saga-worker';

/**
 * create a new DataFlow instance
 * 
 * what is `DataFlow`?
 * 
 * DataFlow is a util for mix `action`,`effects`, `saga`, and `reducer`.
 * There is some method and properties at DataFlow instance:
 * 
 * - actions<object>: actions dict, like: 
 *  {
 *    'articleCreate': () => {
 *      return {
 *        type: 'ARTICLE.CREATE'
 *      } 
 *    }
 *  }
 * - reducer<object>: reducer dict, like:
 *  {
 *    article: function (state, action) => {}
 *  }
 * - effects<object>: effect dict, like:
 *  {
 *    'articleCreateEffect': () => {},
 *    'articleDeleteEffect': () => {},
 *  }
 */

export enum DataFlowSuppotTypes {
  list = 'list',
  detail = 'detail'
}

export interface DataFlowOptions {
  type?: string,
  api?:string,
  state?:any,
  effects?: any,
  reducers?: any,
  autoTasks?: boolean,
  autoStore?: boolean,
  resCheck?: ResCheck
}

export interface CreateDataFlow {
  (scope: string, actionNames: string[], options?:DataFlowOptions): DataFlow
}

export const DEFAULT_DATAFLOW_OPTIONS = {
  type: DataFlowSuppotTypes.list,
  autoTasks: true,
  autoStore: true
}

interface MultipleOptions {
  actions: any[],
  reducers: any[],
  effects: any[],
  sagaWorkers: any[],
  watchers: any[]
}


export class DataFlow {

  readonly __scope__:string;
  readonly __actionNames__: string[];
  readonly __options__: DataFlowOptions;
  readonly actions: Actions;
  readonly effects: Effects;
  readonly state: any;
  readonly reducer: ReducerDict;
  readonly sagaWorkers: null | SagaWorkersDict;
  readonly watchers: Watcher[];
  readonly tasks: undefined | null | Tasks;
  readonly store: undefined | Store;
  readonly dispatch: undefined | Dispatch;

  constructor(scope:string, actionNames: string[],options:DataFlowOptions=DEFAULT_DATAFLOW_OPTIONS) {
    this.__scope__ = scope;
    this.__actionNames__ = actionNames;
    this.__options__ = options;
    this.actions = this.createActions();
    this.effects = this.createEffects();
    
    this.sagaWorkers = this.createSagaWorkers();
    this.state = this.createState();

    this.watchers = this.createWatchers();
    this.reducer = this.createReducer();

    const { autoTasks, autoStore } = options;
    
    if (autoTasks) {
      this.tasks = this.createTasks();
    }

    if (autoStore) {
      this.store = this.createStore();
    }

    // set dispatch
    if (this.store) {
      this.dispatch = this.store.dispatch;
    }

    // bind actions
    if (this.store) {
      this.actions = bindActionCreators(this.actions, this.dispatch);
    }
  }

  private createActions():Actions {
    return createActions(this.__scope__, this.__actionNames__);
  }

  private createEffects():Effects {
    const options = this.__options__;
    const params:any = options ? {
      api: options.api
    } : {};
    let effects = createEffects(this.__scope__, this.__actionNames__, params);
    
    // 使用配置中的同名 effect 替换默认生成的 effects
    if (options && options.effects) {
      for(let name in options.effects) {
        effects[name] = options.effects[name];
      }
    }

    return effects;
  }

  /**
   * tips:这里的 actions 是 dataFlow 实例属性中的 actions
   */
  private createSagaWorkers() {
    if (!this.actions) {
      errorLog('createSagaWorkers', 'There is no actions in DataFlow instance', 'create-data-flow');
      return null;
    }

    if (!this.effects) {
      errorLog('createSagaWorkers', 'There is no effects in DataFlow instance', 'create-data-flow');
      return null;
    }
    const { resCheck } = this.__options__;
    return createSagaWorks(this.actions, this.effects, resCheck);
  }

  private createWatchers(effectFn:any=takeLatest) {
    if (!this.sagaWorkers) {
      errorLog('createWatchers', 'There is no sagaWorkers in DataFlow instance', 'create-data-flow');
      return null;
    }

    return Object.keys(this.sagaWorkers).map((sagaName:string) => {
      const actionName = sagaName.replace(/SagaWorker$/gi, '');
      const action = this.actions[actionName];
      const watchAction = action[action.__actionName__];
      const sagaWorker = this.sagaWorkers[sagaName];
      return createWatcher(watchAction, sagaWorker, effectFn);
    })
  }

  private createState() {
    const state = this.__options__ ? this.__options__.state : {};
    const stateName = createName(this.__scope__, 'state');
    return createState(this.__scope__, this.actions, state)
  }

  private createReducer() {
    const options = this.__options__;
    const type = options && options.type ? options.type : 'list';
    const reducers = options ? options.reducers : {};
    // 同名 reducer 会覆盖
    const _reducers = Object.assign(createCommonReducer(type, this.actions), reducers);
    return createReducer(this.__scope__, this.actions, this.state, _reducers);
  }

  private createTasks() {
    if (!this.watchers) {
      errorLog('createTasks', 'There is no watchers in DataFlow instance', 'create-data-flow');
      return null
    }

    return createTasks(this.watchers);
  }

  private createStore() {
    if (!this.watchers) {
      errorLog('createStore', 'There is no watchers in DataFlow instance', 'create-data-flow');
      return null;
    }
    const reducer = combineReducers(this.reducer);
    return configureStore(reducer, this.tasks);
  }

}

const createDataFlow:CreateDataFlow = function(scope, actionNames, options:DataFlowOptions):any {
  const df = new DataFlow(scope, actionNames, options);
  return df;
}

export default createDataFlow;