import { errorLog } from 'market-utils';
import createFetchEffect from "./create-fetch-effect";

/**
 * create effect
 * tips: effect must a function that return Promise
 */


const createEffect = (effect?:Function, params?:any):Function => {
  if (!effect) {
    const { method, api } = params;
    if (!method && !api) {
      errorLog('createEffect', '默认使用 createFetchEffect, 但是没有找到 method 和 api', 'create-effect');
    }
    return createFetchEffect(method, api);
  }
  return effect.call(null, params);
}


export default createEffect