export interface CreateEffectName {
    (scope: string, action: string): string;
}
declare const createEffectName: CreateEffectName;
export default createEffectName;
