export interface CreateName {
    (scope: string, action: string, ext?: string): string;
}
declare const createName: CreateName;
export default createName;
