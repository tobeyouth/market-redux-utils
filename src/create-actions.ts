import { camelCase } from "market-utils";
import createAction from './create-action';
import { ActionFunction } from "./const";

/**
 * create actions dict by scope and action name
 * like this:
 * {
 *    'articleCreate': Action,
 *    'articleDelete': Action
 * }
 */

export interface Actions {
  [key: string]: ActionFunction
}

export interface CreateActions {
  (scope: string, actionNames: string[]): Actions
}

const createActions:CreateActions = (scope:string, actionNames:string[]):Actions => {
  return actionNames.reduce((r:any, actionName:string) => {
    return Object.assign(r, {
      [camelCase(`${scope}-${actionName}`)]: createAction(scope, actionName)
    })
  }, {})
}

export default createActions;