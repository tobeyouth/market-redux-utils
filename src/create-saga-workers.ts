import { camelCase } from "market-utils";
import createSagaWorker, { ResCheck } from './create-saga-worker';
import createName from './create-name';


/**
 * create saga dict by Actions[]
 * like this:
 * {
 *    'articleCreateSaga': ()* => {},
 *    'articleDeleteSaga': ()* => {}
 * }
 */

export interface SagaWorkersDict {
  [sagaWorkerName: string]: GeneratorFunction
}

export interface CreateSagaWorkers {
  (actions:any, effects:any, resCheck:ResCheck): SagaWorkersDict
}

const createSagaWokers:CreateSagaWorkers = (actions, effects, resCheck) => {
  return Object.keys(actions).reduce((r:any, name) => {
    const act = actions[name];
    const effect = effects[createName(name, 'effect')];
    return Object.assign(r, {
      [createName(name, 'saga-worker')]: createSagaWorker(act, effect, resCheck)
    })
  }, {});
}

export default createSagaWokers;