export interface EffectFn {
    (method: string, api: string, params?: any): Promise<any>;
}
declare const createFetchEffect: (method: string, api: string) => (params?: any) => Promise<any>;
export default createFetchEffect;
