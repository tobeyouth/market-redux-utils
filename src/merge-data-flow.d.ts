import { DataFlow } from './create-data-flow';
declare class MultipleDataFlow {
    readonly __scope__: string[];
    readonly actions: any;
    readonly effects: any;
    readonly state: any;
    readonly reducer: any;
    readonly sagaWorkers: any;
    readonly watchers: any[];
    readonly tasks: any;
    readonly store: any;
    readonly dispatch: null;
    readonly mergeReducers: any;
    constructor(instances: DataFlow[]);
}
declare const mergeDataFlow: (...instances: DataFlow[]) => MultipleDataFlow;
export default mergeDataFlow;
