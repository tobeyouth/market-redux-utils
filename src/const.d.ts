import { AnyAction } from "redux";
export declare enum ACTION_METHODS {
    fetch = "get",
    update = "put",
    save = "put",
    create = "post",
    delete = "delete",
    remove = "delete"
}
export interface ActionFunction {
    (payload?: any): any;
    [type: string]: any;
    __async__: boolean;
    __scope__: string;
    __actionName__: string;
    success: string;
    fail: string;
    pending: string;
}
export interface Action extends AnyAction {
    payload?: any;
}
