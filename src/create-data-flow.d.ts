import { Actions } from "./create-actions";
import { Effects } from "./create-effects";
import { SagaWorkersDict } from "./create-saga-workers";
import { Tasks } from "./create-tasks";
import { Watcher } from "./create-watcher";
import { ReducerDict } from "./create-reducer";
import { Dispatch, Store } from "redux";
import { ResCheck } from './create-saga-worker';
export declare enum DataFlowSuppotTypes {
    list = "list",
    detail = "detail"
}
export interface DataFlowOptions {
    type?: string;
    api?: string;
    state?: any;
    effects?: any;
    reducers?: any;
    autoTasks?: boolean;
    autoStore?: boolean;
    resCheck?: ResCheck;
}
export interface CreateDataFlow {
    (scope: string, actionNames: string[], options?: DataFlowOptions): DataFlow;
}
export declare const DEFAULT_DATAFLOW_OPTIONS: {
    type: DataFlowSuppotTypes;
    autoTasks: boolean;
    autoStore: boolean;
};
export declare class DataFlow {
    readonly __scope__: string;
    readonly __actionNames__: string[];
    readonly __options__: DataFlowOptions;
    readonly actions: Actions;
    readonly effects: Effects;
    readonly state: any;
    readonly reducer: ReducerDict;
    readonly sagaWorkers: null | SagaWorkersDict;
    readonly watchers: Watcher[];
    readonly tasks: undefined | null | Tasks;
    readonly store: undefined | Store;
    readonly dispatch: undefined | Dispatch;
    constructor(scope: string, actionNames: string[], options?: DataFlowOptions);
    private createActions;
    private createEffects;
    private createSagaWorkers;
    private createWatchers;
    private createState;
    private createReducer;
    private createTasks;
    private createStore;
}
declare const createDataFlow: CreateDataFlow;
export default createDataFlow;
