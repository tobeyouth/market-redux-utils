export interface CreateActionName {
    (scope: string, action: string, status?: string): string;
}
declare const createActionName: CreateActionName;
export default createActionName;
