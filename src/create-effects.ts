import { camelCase } from "market-utils";
import createEffect from './create-effect';
import createName from './create-name';
import { ACTION_METHODS } from './const';

/**
 * create effects by scope & action
 * return dict like:
 * {
 *    articleCreateEffect: () => {},
 *    articleDeleteEffect: () => {}
 * }
 */

// helper
export function methodHelper(action:string):string {
  return ACTION_METHODS[action as keyof typeof ACTION_METHODS] || 'get';
}

export interface Effects {
  [key: string]: Function
}

const createEffects = (scope:string, actions:string[],payload?:any, effect?:Function, ):Effects => {
  return actions.reduce((r:any, action:string):any => {
    const method = methodHelper(action);
    const _payload = Object.assign({}, payload, {method});
    return Object.assign(r, {
      [createName(scope, action, 'effect')]: createEffect(effect, _payload)
    })
  }, {});
}

export default createEffects;