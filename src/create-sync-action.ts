/**
 * create sync action
 */
import { ActionFunction, Action } from './const';

export interface SyncAction {
  (scope: string, action: string): ActionFunction
}

const createSyncAction:SyncAction = function (scope, action) {
  const _action = <ActionFunction>function(payload: any):Action {
    return {
      type: [scope, action].filter((k) => { 
        return !!k
      }).join('.').toUpperCase(),
      payload
    }
  }
  
  _action.__async__ = false;
  _action.__scope__ = scope;
  _action.__actionName__ = action;
  
  return _action;
}

export default createSyncAction