import { Reducer } from 'redux';
import { StateAny } from './create-state';
export interface ReducerDict {
    [reduerName: string]: Reducer;
}
export interface CreateReducer {
    (scope: string, actions: any, state: StateAny, reducers?: any): ReducerDict;
}
declare const createReducer: CreateReducer;
export default createReducer;
