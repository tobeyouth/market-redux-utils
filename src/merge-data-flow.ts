import { combineReducers, bindActionCreators } from 'redux';
import createTasks from "./create-tasks"
import { DataFlow } from './create-data-flow';
import configureStore from "./configure-store";;

// merge 后返回的新实例 
class MultipleDataFlow {
  
  readonly __scope__:string[];
  readonly actions: any;
  readonly effects: any;
  readonly state: any;
  readonly reducer: any;
  readonly sagaWorkers: any;
  readonly watchers: any[];
  readonly tasks: any;
  readonly store: any;
  readonly dispatch: null;
  readonly mergeReducers: any;

  constructor (instances:DataFlow[]) {
    const scopes = instances.map((df) => {
      return df.__scope__;
    });
    // 合并 actions
    this.actions = instances.reduce((r, df) => {
      const _actions = df.actions;
      // 同名 actions 要进行 warning
      const [hasSameKey, key] = checkSameKey(Object.keys(r), Object.keys(_actions));
      if (hasSameKey) {
        console.warn(`There is same action key: ${key}, in instances`);
      }
      return Object.assign(r, { ..._actions })
    }, {});
    
    // 合并 effects
    this.effects = instances.reduce((r, df) => {
      const _effects = df.effects;
      // 同名 effects 进行 warning
      const [hasSameKey, key] = checkSameKey(Object.keys(r), Object.keys(_effects));
      if (hasSameKey) {
        console.warn(`There is same effect key: ${key}, in instances`);
      }
      return Object.assign(r, { ..._effects});
    })

    // 合并 state
    this.state = instances.reduce((r, df) => {
      const _state = df.state;
      return Object.assign(r, { [`${_state.scope}State`]: _state });
    }, {});

    // 合并 reducers
    const reducer = instances.reduce((r, df) => {
      const _reducer = df.reducer;
      const [hasSameKey, key] = checkSameKey(Object.keys(r), Object.keys(_reducer));
      if (hasSameKey) {
        console.warn(`There is same reducer key: ${key}, in instances`);
      }
      return Object.assign(r, _reducer);
    }, {});

    this.reducer = reducer;

    // 合并 sagaWorkers
    this.sagaWorkers = instances.reduce((r, df) => {
      const _sagaWorkers = df.sagaWorkers;
      const [hasSameKey, key] = checkSameKey(Object.keys(r), Object.keys(_sagaWorkers));
      if (hasSameKey) {
        console.warn(`There is same sagaWorker key: ${key}, in instances`);
      }
      return Object.assign(r, { ..._sagaWorkers });
    })

    // 合并 watchers
    this.watchers = instances.reduce((r, df) => {
      const _watchers = df.watchers;
      return r.concat(_watchers);
    }, []);

    // 创建 task
    this.tasks = createTasks(this.watchers);

    // 创建 store
    this.store = configureStore(combineReducers(this.reducer), this.tasks);

    // set dispatch
    this.dispatch = this.store.dispatch;
    // bind actions
    this.actions = bindActionCreators(this.actions, this.dispatch);
  }
}

// helper
function checkSameKey(a:string[], b:string[]):[boolean, string] {
  const sameKey:string = a.find((item:any):any => {
    return b.find((_item:any):any => {
      return _item === item;
    })
  })
  return [!!sameKey, sameKey];
}

const mergeDataFlow = (...instances:DataFlow[]):MultipleDataFlow => {
  return new MultipleDataFlow(instances);
};

export default mergeDataFlow;