import { ActionFunction } from './const';
export interface AsyncAction {
    (scope: string, actionName: string, proxy?: Function): ActionFunction;
}
declare const createAsyncAction: AsyncAction;
export default createAsyncAction;
