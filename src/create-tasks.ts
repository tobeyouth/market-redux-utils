import { fork, ForkEffect } from 'redux-saga/effects';
import { Watcher } from './create-watcher';

/**
 * return list of forked watchers
 */
export type Tasks = () => IterableIterator<Array<ForkEffect>>;

const createTasks = (watchers:Array<Watcher>):Tasks => {
  return function* () {
    yield watchers.map((w) => {
      return fork(w);
    })
  }
}
export default createTasks;