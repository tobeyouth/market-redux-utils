import { ActionFunction } from './const';
declare const _default: (scope: string, action: string, proxy?: boolean | Function) => ActionFunction;
export default _default;
