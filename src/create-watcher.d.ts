import { takeEvery, ForkEffect } from "redux-saga/effects";
export declare type Watcher = () => IterableIterator<ForkEffect>;
declare const _default: (action: string, worker: GeneratorFunction, effectFn?: typeof takeEvery) => Watcher;
export default _default;
