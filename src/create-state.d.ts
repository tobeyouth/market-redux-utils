export interface State {
    scope: string;
    error: any;
    data: any;
    [status: string]: any;
}
export interface StateAny {
    query?: any;
    [prop: string]: any;
}
export interface CreateState {
    (scope: string, actions: any, state?: any): StateAny;
}
declare const createState: CreateState;
export default createState;
