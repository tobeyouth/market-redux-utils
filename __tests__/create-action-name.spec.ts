import createActionName from './../src/create-action-name';

describe('create-reducer-name', () => {
  const scope = 'article';
  const action = 'create';
  const status = 'success';

  test('all params name', () => {
    expect(createActionName(scope, action, status)).toEqual('ARTICLE.CREATE.SUCCESS');
  })
  test('wihtout status', () => {
    expect(createActionName(scope, action)).toEqual('ARTICLE.CREATE');
  })
})