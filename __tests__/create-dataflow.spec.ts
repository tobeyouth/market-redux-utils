import createDataFlow from './../src/create-data-flow';
import { DEFAULT, PENDING } from 'market-utils';
import { bindActionCreators } from 'redux';


describe('create-data-flow', () => {
  const api = 'http://moro.dapps.douban.com/api/areas';
  const scope = 'article';
  const actionNames = ['create', 'update', 'fetch'];
  const df = createDataFlow(scope, actionNames, { 
    api, 
    autoTasks: true, 
    autoStore: true 
  });

  test('instance properties', () => {
    expect(df).toHaveProperty('__scope__');
    expect(df).toHaveProperty('__actionNames__');
    expect(df).toHaveProperty('actions');
    expect(df).toHaveProperty('effects');
    expect(df).toHaveProperty('tasks');
    expect(df).toHaveProperty('reducer');
    expect(df).toHaveProperty('sagaWorkers');

    expect(df.__scope__).toEqual(scope);
    expect(df.__actionNames__).toEqual(actionNames);
  })

  // 使用 action 更新状态
  test('change state by instance.actions', () => {
    const { actions } = df;
    actions.articleFetch();
    const props = df.store.getState();
    const { articleState } = props;
    expect(articleState.fetchStatus).toBe(PENDING);
  })
})
