import { Task } from 'redux-saga';
import createTasks from './../src/create-tasks';
import createWather from './../src/create-watcher';
import createAction from './../src/create-action';
import createFetchEffect from './../src/create-fetch-effect';
import createSagaWorker from './../src/create-saga-worker';


describe('create tasks', () => {
  const action = createAction('article', 'create');
  const effect = createFetchEffect('get', '/api');
  const sagaWorker = createSagaWorker(action, effect);
  const watcher = createWather(action.create, sagaWorker);
  const tasks = createTasks([watcher]);
  const generator = tasks();

  test('tasks return a generator', () => {
    expect(generator).toHaveProperty('next');
  })
})