import createActions from './../src/create-actions';
import createState from './../src/create-state';
import createReducer from './../src/create-reducer';
import { PENDING } from 'market-utils';

describe('create-reducer', () => {
  const scope = 'article';
  const actions = createActions(scope, ['create', 'fetch', 'delete', 'update']);
  const state = createState(scope, actions, {
    total: 0
  })
  const reducers = createReducer(scope, actions, state);
  const reducerName = `${scope}State`;
  const reducer = reducers[reducerName];
  
  test('create real function', () => {
    expect(reducers).toHaveProperty(reducerName);
    expect(reducer).toBeInstanceOf(Function);
  })

  test('reducer state', () => {
    expect(reducer()).toHaveProperty('createStatus');
    expect(reducer()).toHaveProperty('fetchStatus');
    expect(reducer()).toHaveProperty('deleteStatus');
    expect(reducer()).toHaveProperty('updateStatus');
  })

  test('support custom state', () => {
    expect(reducer()).toHaveProperty('total');
  })

})