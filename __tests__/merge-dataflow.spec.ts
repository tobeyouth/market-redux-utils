import mergeDataFlow from './../src/merge-data-flow';
import createDataFlow from './../src/create-data-flow';
import { DEFAULT, PENDING, camelCase } from 'market-utils';
import createActionName from '../src/create-action-name';

describe('merge dataflow instance', () => {
  const apiFoo = '/api/foo';
  const apiBar = '/api/bar';
  const scopeFoo = 'foo';
  const scopeBar = 'bar';
  const actions = ['fetch', 'create'];
  const dfFoo = createDataFlow(scopeFoo, actions, {
    api: apiFoo,
    autoStore: true,
    autoTasks: true
  })
  const dfBar = createDataFlow(scopeBar, actions, {
    api: apiBar,
    autoStore: true,
    autoTasks: true
  })
  const multiple = mergeDataFlow(dfFoo, dfBar);
  
  test('merge all action functions', () => {
    const _actions = [scopeFoo, scopeBar].reduce((r, s) => {
      return r.concat(actions.map((a) => {
        return camelCase(`${s}-${a}`)
      }))
    }, []);
    expect(Object.keys(multiple.actions)).toEqual(_actions);
  })

  test('merge all state', () => {
    const state = multiple.store.getState();
    expect(state).toHaveProperty('fooState');
    expect(state).toHaveProperty('barState');
  })

  test('change by action', () => {
    const { actions } = multiple;
    const fooAction = actions.fooCreate();
    const barAction = actions.barCreate();

    expect(fooAction.type).toBe('FOO.CREATE');
    expect(barAction.type).toBe('BAR.CREATE');

    const props = multiple.store.getState();

    const { fooState, barState } = props;

    expect(fooState.createStatus).toBe(PENDING);
    expect(barState.createStatus).toBe(PENDING);
  })
})