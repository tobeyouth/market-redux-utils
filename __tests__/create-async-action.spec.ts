import createAsyncAction from './../src/create-async-action';

describe('create-async-action', () => {
  const fooAction = createAsyncAction('foo', 'create');

  test('action is a function', () => {
    expect(typeof(fooAction)).toBe('function');
  })

  test('action return with type', () => {
    expect(fooAction()).toHaveProperty('type');
  })

  test('action return with payload', () => {
    expect(fooAction({'foo': 'FOO'})).toHaveProperty('payload');
    expect(fooAction({'foo': 'FOO'}).payload).toEqual({'foo':'FOO'});
  })

  test('action return type is equal actionname', () => {
    expect(fooAction().type).toBe('FOO.CREATE');
  })
  
  test('action has create', () => {
    expect(fooAction).toHaveProperty('create');
    expect(fooAction.create).toBe('FOO.CREATE');
  })

  test('action has success', () => {
    expect(fooAction).toHaveProperty('success');
    expect(fooAction.success).toBe('FOO.CREATE.SUCCESS');
  })

  test('action has fail', () => {
    expect(fooAction).toHaveProperty('fail');
    expect(fooAction.fail).toBe('FOO.CREATE.FAIL');
  })
})