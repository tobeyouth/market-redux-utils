import createAction from './../src/create-action';
import createSaga from './../src/create-saga-worker';
import createFetchEffect from './../src/create-fetch-effect';

describe('create-saga-worker', () => {
  const action = createAction('article', 'create');
  const effect = createFetchEffect('get', '/api');
  const sagaWorker = createSaga(action, effect);
  const generator = sagaWorker(action.create);

  test('saga worker is generator function', () => {
    expect(generator).toHaveProperty('next');
  });

})