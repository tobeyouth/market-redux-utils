import createWather from './../src/create-watcher';
import createAction from './../src/create-action';
import createFetchEffect from './../src/create-fetch-effect';
import createSagaWorker from './../src/create-saga-worker';

describe('create watcher', () => {
  const action = createAction('article', 'create');
  const effect = createFetchEffect('get', '/api');
  const sagaWorker = createSagaWorker(action, effect);
  const watcher = createWather(action.create, sagaWorker);
  const generator = watcher();
  
  test('watcher is generator function', () => {
    expect(generator).toHaveProperty('next');
  })
})