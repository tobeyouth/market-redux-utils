import createSyncAction from './../src/create-sync-action';

describe('create-sync-action', () => {
  const fooAction = createSyncAction('foo', 'create');

  test('action is a function', () => {
    expect(typeof(fooAction)).toBe('function');
  })

  test('action return with type', () => {
    expect(fooAction()).toHaveProperty('type');
  })

  test('action return with payload', () => {
    expect(fooAction({'foo': 'FOO'}).payload).toHaveProperty('foo');
  })

  test('action return type is equal actionname', () => {
    expect(fooAction().type).toBe('FOO.CREATE');
  })
})