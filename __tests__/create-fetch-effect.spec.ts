import createFetchEffect from './../src/create-fetch-effect';
import { SUPPORT_METHODS } from 'market-utils';

describe('create-fetch-effect', () => {
  test('return promise', () => {
    const effect = createFetchEffect('get', '/test');
    expect(effect()).toBeInstanceOf(Promise);
  })
})