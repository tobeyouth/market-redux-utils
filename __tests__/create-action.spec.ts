import createAction from './../src/create-action';

describe('create-action', () => {
  const syncAction = createAction('sync', 'action', true);
  const asyncAction = createAction('async', 'action');

  test('sync-action', () => {
    expect(syncAction.__async__).toBe(false);
  })

  test('async-action', () => {
    expect(asyncAction.__async__).toBe(true);
  })
})