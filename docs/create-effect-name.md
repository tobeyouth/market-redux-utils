## create-effect-name

根据 `scope`和`action`生成对应的 `effect`的 name，用于配置`DataFlow`的 `options.effects`, 例如：

```
const df = createDataFlow({
  effects: {
    [createEffectName('article', 'create')] (payload) => {}
  }
})
```