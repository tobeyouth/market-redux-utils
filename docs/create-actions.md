## create-actions

批量调用 `create-action`, 返回一个 map(根据传入的 `scope`和 `actions`, 生成 key, value 则为生成的`action-function`)

```
// createActions(scope:string, articles:string[])

import createActions from './create-actions';

const articleActions = createActions('article', ['create', 'delete']);
// articleActions => {
  articleCreate: Action
  articleDelete: Action
}
```
