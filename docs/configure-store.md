## configure-store

新建一个 `store`, 支持传入 `reducer`和`saga-watchers`

```
import reducer from './reducer';
import watchers from './sagas';
import configureStore from './configure-store';

const sotre = configureStore(reducer, watchers);
```