## create-sync-action

创建一个同步的 `action`，例如：

```
import createSyncAction from './create-sync-action';

const pageInit = createSyncAction('page', 'init');

// pageInit => () => (params) => {
  return {
    type: 'PAGE.INIT'
    ...params
  }
}
```