## create-async-action

创建一个异步的 action。


### 使用场景

主要针对场景是在开发中，会经常会遇到异步事件，通常情况下，我们是使用 `create-fetch-types` 这个方法，创建一个带有不同状态的 `actionMaps`，然后分别在 `action`, `saga` 中 dispatch 对应的 type; 在 `reducer`中进行处理。

使用以上的这种方法，需要新建 `action-types`和`action`两种不同的文件，同时在不同的地方引用不同的文件，例如：在 view 中引入 `action`，在 `action`，`saga`和`reducer`中引入 `action-types`。

例如：

```
// action-type.js
import { createFetchTypes } from 'market-utils';

export const ARTICLE_CREATE = createFetchTypes('article', 'create');

// action.js
import { ARTICLE_CREATE } from './action-types';

export function articleCreate() {
  return {
    type: ARTICLE_CREATE.create
  }
}

// reducer.js
import { ARTICLE_CREATE } from './action-types';

export default (state, action) {
  switch action.type {
    case ARTICLE_CREATE.create:
      ...
    default:
      return state
  }
}

// saga
import { ARTICLE_CREATE } from './action-types';

function* articleCreateSaga() {
  return yield put({type: ARTICLE_CREATE.create})
}

// view
import React, { Component } from 'react';
import { articleCreate } from './actions';

class View extends Component {

  componentDidMount () {
    articleCreate();
  }
}
```

`create-async-action`把创建 `action-type`和`action`整合到了一起，只创建一个 `action`，在以上的场景里，需要用到 `action-types`和`action`，都可以使用`create-async-action`创建的 `action`即可。

例如：

```
// actions.js
import { createAsyncAction } from 'market-redux-utils';
export const articleCreate = createAsyncAction('article', 'create');

// reducer.js
import { articleCreate } from './action';

export default (state, action) {
  switch action.type {
    case articleCreate.create:
      ...
    default:
      return state
  }
}

// saga
import { articleCreate } from './action';

function* articleCreateSaga() {
  return yield put({type: articleCreate.create})
}

// view
import React, { Component } from 'react';
import { articleCreate } from './actions';

class View extends Component {

  componentDidMount () {
    articleCreate();
  }
}
```

### 参数 & 返回值

```
const action = createAsyncAction(scope:string, action:string, proxy: function);
```

##### 参数
- scope: 操作对象
- action: 操作名称
- proyx: 对传入参数的处理（此方法必须有返回值）

##### 返回值

返回的 `action`本身是个可执行的 `function`, **可以传入一个 object  作为参数**, 该 `object` 会被展开添加到 `action`执行完的返回值里，例如

```
const r = action({'foo': 'FOO', 'bar': 'BAR'});
// r => {
  type: 'scope.action',
  foo: 'FOO',
  bar: 'BAR'
}
```

同时 `action`还具有 `<action>`, `pending`, `success`, `fail` 四种状态值，可以在需要使用的地方直接调用.