## create-fetch-effect

使用 `fetch` 创建一个 `effect`，返回 Promise 形式的 `Fetch`实例

```
import createFetchEffect from './create-fetch-effect';

const fooEffetct = createFetchEffect('get', '/api', {'foo': 'FOO'});
```
