## create-effect

生成 effect 方法，支持的参数有：
```
createEffect(effect?:Function, params?:any);
```
- effect: 自定义的 `effect-function`.
- params: `effect-function` 执行时传入的参数

支持自定义 `effect-function`, 如果自定义 `effect-function`, 需要注意的是，这个 `effect-function` 必须返回一个 `Promise` 对象；

如果不自定义 `effect-function`的话，将默认从 `params` 中获取 `method`和 `api` 这两个字段，同时调用 `Fetch` 方法。