## create-saga-worker

新建一个 saga woker, 需要传入以下参数:


- actionType<FetchTypes>: FetchTypes 的实例, 包含`success`和`fail`
- effect<GeneratorFunction>: effect 方法，需要是 generator 函数
- check<Function>: 判断请求是否成功的方法，默认是通过`res.r === 0` 来判断

返回一个 generator 函数，该函数**接收一个参数**，改参数既为 `action` 中返回的结果，参数结构为：

```
{
  type: <string>,
  ...params
}
```

返回的 generator 函数中，会根据 `check` 返回的结果，触发 `actionType.success`或`actionType.fail`。
