## data flow

`DataFlow` 是一个把 redux 开发流程中各个部分聚合在一起的对象。

### 一些概念

- scope: `data`的实体对象
- action: 针对 `scope`的交互动作，常用的有 `create`, `update`, `delete`, `fetch`

### 新建 DataFlow 实例

`createDataFlow`方法支持三个参数，分别是:

- scope
- actions: action 的列表
- options: 一些其他配置，具体内容可以看下面的这个 interface

```
interface DataFlowOptions {
  type?:string,
  api?:string,
  state?:any,
  effects?: any,
  reducers?: any,
  autoTasks?: boolean,
  autoStore?: boolean
}
```

#### DataFlowOptions

- type: store 中存储的 `data`对象的类型，目前支持 `list`和`detail` 两种
- api: 如果该实例对应一个 api，那么可以把这个 api 直接写入到配置参数中；action 触发默认 effects 会向这个 api 发送请求
- state: 默认会创建一个包含 `data`, `error`和各种 `status`的 `STATE`,如果需要传入更多的字段，可以写入到 `state`中，同名的字段会优先使用传入的 `state`中的值。
- effects: DataFlow 新建实例时，默认会根据 `actions`创建一些对应的 `effect`，如果默认创建的 `effect` 不能满足需求，可以自定义 `effect`, 自定义的 `effect` 的命名必须是根据 `cammelCase(${scope}-${action}-${effect})`的格式，也可以通过 [create-effect-name](./create-effect-name.md) 来创建命名，例如

```
const df = createDataFlow(scope, actions, {
  effects: {
    [createEffectName(scope, action)] (payload) => {}
  }
})
```
- reducers: DataFlow 新建实例时，默认会根据`scope`和`action`新建一个默认的 `reducer`，但是这个默认的 `reducer`只会更新 `status`和`data`；如果不能满足需求的话，可以自定义一些`reducer`处理数据，自定义的 `reducer`会在默认的 reudcer 处理完之后，继续处理数据。自定义的 `reducer`必须以大写`${scope}.${actionType}`方式命名，如`ARTICLE.CREATE.SUCCESS`，也可以通过 [create-action-name](./create-action-name.md)的方式来新建命名，例如

```
const df = createDataFlow(scope, actions, {
  reducers: {
    [createActionName(scope, 'create', 'success')] (state) => { return state } 
  }
})
```
需要注意的是，自定义的 `reducer`必须返回 state，否则将会产生错误。

- autoTasks: 是否自动 fork 实例的 watchers
- autoStore: 是否自动创建实例的 store, 如果自动创建实例的 store，那么实例的 `actions`将会自动绑定 `store`的 `dispatch`，在 view 中将不需要再使用 `bindActionCreators`绑定了。


##### 属性和方法

每个 `DataFlow` 的实例，都包含了以下属性：

- actions: 通过 `create-actions`方法创建的 dict
- effects: 通过 `create-effects` 方法创建的 dict
- reducers: 通过 `create-reducer` 方法创建的`reducer`, 会根据传入的 `action`自动生成一些 `status`(例如`createStatus`, `fetchStatus`这种)，对应 type 不同的 action 触发时，会更新 `status`的状态；同时默认所有数据都存放在 `data`这个字段中（不管是 object 还是 array）;
另外，为了存取列表方便，还加了 `query`和`total`两个字段。
- sagaWorkers(既 redux-saga 概念里的, worker; 因为担心会跟其他 worker 重名，所以加了个 saga 的前缀): 通过 `create-saga-workers`创建的 dict
- watchers： 通过`create-wather` 创建的 `redux-saga watcher`列表;每一个 watcher 都是一个 generator function.
- tasks: 通过 `create-tasks`创建的 generator function, 会迭代并 `fork` `watchers`列表中的 watcher
- store: 通过`configure-store`创建的 store
- reducer: 通过 `create-reducer` 创建的 `reducer`
- dispatch: 如果默认创建 `store`，则实例的`dispatch`为 `store.dispatch`, 否则将为 `undefined`


`DataFlow` 包含以下静态方法

- merge(<DataFlowInstance, DataFlowInstance>): 合并多个 `DataFlowInstance`, 并返回一个新的 `DataFlowInstance`

#### 自动添加的 reducer

为了方便起见，默认会添加一些 `reducer`，分别对应 `actions`中的各个状态，会根据 `DataFlowOptions`中传入的`type`和 `action.query` 进行不同的操作，如果不满足需求，可以直接在 `DataFlowOptions`中的`reducers`参数中，直接替换该 `reducer`即可。



### merge-data-flow

因为`DataFlow`的实例，每一个只对应一个 `reducer`，但是在实际开发中，可能会有一个页面对应多个 reducer 的情况，所以提供了一个  `mergeDataFlow` 的方法，可以将多个 `DataFlow`的实例合并成一个新的 `MutipleDataFlow`实例，同样具有`DataFlow`的属性和方法。

### tips

- 目前 DataFlow 中的 action, 都只支持**传入一个参数**，这个参数会在整个数据流中，被放到 `payload`字段中，可以在 `effects`和`reducers`中获取并处理。
- 为了保持跟使用体验统一，所以 DataFlow 实例化的过程中，也会进行 `combineReducers`操作，简单来说，就是如果执行 `df.store.getState()`，将会获得 `{ ${scope}State: state }` 这样的一个 object, 其中的 value 才是 reducer 真正更新的 state。
