## create-reducer-name

创建一个符合 `DataFlow` 命名规则的 `reduerName`，通常用于配置 `DataFlow`的初始化`options.reducers`选项.

例如：

```
import createDataFlow from 'create-data-flow';
import createReducerName from 'create-reducer-name';

const df = createDataFlow({
  reducers: {
    [createReducerName(scope, action, status)] (state) => { return state }
  }
})
```