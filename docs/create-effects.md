## create-effects

批量调用 `create-effect`, 返回一个 dict（根据传入的 `scope`和`actions`生成 key, value 则对应生成 `effect-function`）

```
// createEffects(scope:string, actions:string[], effect?:Function, params?:any)
const effects = createEffects('article', ['create', 'update']);

// effects => {
  articleCreateEffect: effect-function,
  articleUpdateEffect: effect-function,
}
```

