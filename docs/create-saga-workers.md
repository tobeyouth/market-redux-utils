## create-saga-wokers

批量调用 `create-saga-worker`, 返回一个 map(根据传入的 `actions`和`effects`自动生成 key, value 为生成的 generator function).


```
import createSagaWorkers from './create-saga-wokers';
import createActions from './create-actions';
import createEffects from './create-effects';

const actions = createActions('article', ['create', 'update']);
const effects = createEffects('article', actions);
const sagaWorkers = createSagaWorkers(actions, effects);

// sagaWorkers => {
  articleCreateSaga: generator function,
  articleUpdateSaga: genertator function
}
```

