## create-action

一个调用 `create-async-action`和`create-sync-action`的统一入口，默认使用 `async`方式。

```
import createAction from './create-action';

// async
const asyncAction = createAction('async', 'fetch');

// sync
const syncAction = createSyncAction('sync', 'init', true);
```

