var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./create-effect", "./create-name", "./const"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var create_effect_1 = __importDefault(require("./create-effect"));
    var create_name_1 = __importDefault(require("./create-name"));
    var const_1 = require("./const");
    function methodHelper(action) {
        return const_1.ACTION_METHODS[action] || 'get';
    }
    exports.methodHelper = methodHelper;
    var createEffects = function (scope, actions, payload, effect) {
        return actions.reduce(function (r, action) {
            var _a;
            var method = methodHelper(action);
            var _payload = Object.assign({}, payload, { method: method });
            return Object.assign(r, (_a = {},
                _a[create_name_1.default(scope, action, 'effect')] = create_effect_1.default(effect, _payload),
                _a));
        }, {});
    };
    exports.default = createEffects;
});
//# sourceMappingURL=create-effects.js.map