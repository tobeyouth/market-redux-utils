var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "market-utils", "./create-fetch-effect"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var market_utils_1 = require("market-utils");
    var create_fetch_effect_1 = __importDefault(require("./create-fetch-effect"));
    var createEffect = function (effect, params) {
        if (!effect) {
            var method = params.method, api = params.api;
            if (!method && !api) {
                market_utils_1.errorLog('createEffect', '默认使用 createFetchEffect, 但是没有找到 method 和 api', 'create-effect');
            }
            return create_fetch_effect_1.default(method, api);
        }
        return effect.call(null, params);
    };
    exports.default = createEffect;
});
//# sourceMappingURL=create-effect.js.map