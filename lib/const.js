(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ACTION_METHODS;
    (function (ACTION_METHODS) {
        ACTION_METHODS["fetch"] = "get";
        ACTION_METHODS["update"] = "put";
        ACTION_METHODS["save"] = "put";
        ACTION_METHODS["create"] = "post";
        ACTION_METHODS["delete"] = "delete";
        ACTION_METHODS["remove"] = "delete";
    })(ACTION_METHODS = exports.ACTION_METHODS || (exports.ACTION_METHODS = {}));
});
//# sourceMappingURL=const.js.map