var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./create-saga-worker", "./create-name"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var create_saga_worker_1 = __importDefault(require("./create-saga-worker"));
    var create_name_1 = __importDefault(require("./create-name"));
    var createSagaWokers = function (actions, effects, resCheck) {
        return Object.keys(actions).reduce(function (r, name) {
            var _a;
            var act = actions[name];
            var effect = effects[create_name_1.default(name, 'effect')];
            return Object.assign(r, (_a = {},
                _a[create_name_1.default(name, 'saga-worker')] = create_saga_worker_1.default(act, effect, resCheck),
                _a));
        }, {});
    };
    exports.default = createSagaWokers;
});
//# sourceMappingURL=create-saga-workers.js.map