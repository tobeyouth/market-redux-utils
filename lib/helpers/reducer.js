var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "market-utils", "./../create-action-name"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var market_utils_1 = require("market-utils");
    var create_action_name_1 = __importDefault(require("./../create-action-name"));
    exports.createCommonReducer = function (type, actions) {
        if (type === 'list') {
            return createListReducers(actions);
        }
        else {
            return createDetailReducers(actions);
        }
    };
    function createListReducers(actions) {
        return Object.keys(actions).reduce(function (r, actName) {
            var _a;
            var act = actions[actName];
            var status = act.__actionName__ + "Status";
            var startAction = create_action_name_1.default(act.__scope__, act.__actionName__);
            return Object.assign(r, (_a = {},
                _a[startAction] = function (state, action) {
                    var _state = __assign({}, state);
                    _state[status] = market_utils_1.PENDING;
                    return _state;
                },
                _a[act.success] = function (state, action) {
                    var _state = __assign({}, state);
                    _state[status] = market_utils_1.SUCCESS;
                    if (_state.data && Array.isArray(_state.data)) {
                        if (action.query && parseInt(action.query.page, 10) === 1) {
                            _state.data = action.res.data;
                        }
                        else {
                            _state.data = _state.data.concat(action.res.data);
                        }
                    }
                    else {
                        _state.data = action.res.data;
                    }
                    if (action.payload && action.payload.page) {
                        _state.query = {
                            page: action.payload.page,
                            page_size: action.payload.page_size
                        };
                    }
                    return _state;
                },
                _a[act.fail] = function (state, action) {
                    var _state = __assign({}, state);
                    _state[status] = market_utils_1.FAIL;
                    return _state;
                },
                _a));
        }, {});
    }
    exports.createListReducers = createListReducers;
    function createDetailReducers(actions) {
        return Object.keys(actions).reduce(function (r, actName) {
            var _a;
            var act = actions[actName];
            var status = act.__actionName__ + "Status";
            var startAction = create_action_name_1.default(act.__scope__, act.__actionName__);
            return Object.assign(r, (_a = {},
                _a[startAction] = function (state, action) {
                    var _state = __assign({}, state);
                    _state[status] = market_utils_1.PENDING;
                    _state.error = '';
                    return _state;
                },
                _a[act.success] = function (state, action) {
                    var _state = __assign({}, state);
                    _state[status] = market_utils_1.SUCCESS;
                    _state.error = '';
                    _state.data = action.res.data;
                    return _state;
                },
                _a[act.fail] = function (state, action) {
                    var _state = __assign({}, state);
                    _state[status] = market_utils_1.FAIL;
                    _state.error = action.error;
                    return _state;
                },
                _a));
        }, {});
    }
    exports.createDetailReducers = createDetailReducers;
});
//# sourceMappingURL=reducer.js.map