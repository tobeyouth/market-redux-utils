(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./reducer"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var reducer_1 = require("./reducer");
    exports.createCommonReducer = reducer_1.createCommonReducer;
    exports.createListReducers = reducer_1.createListReducers;
    exports.createDetailReducers = reducer_1.createDetailReducers;
});
//# sourceMappingURL=index.js.map