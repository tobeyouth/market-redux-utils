(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./create-name", "./create-effect-name", "./create-action-name", "./configure-store", "./create-action", "./create-async-action", "./create-sync-action", "./create-effect", "./create-fetch-effect", "./create-reducer", "./create-saga-worker", "./create-state", "./create-tasks", "./create-watcher", "./create-data-flow", "./merge-data-flow"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var create_name_1 = require("./create-name");
    exports.createName = create_name_1.default;
    var create_effect_name_1 = require("./create-effect-name");
    exports.createEffectName = create_effect_name_1.default;
    var create_action_name_1 = require("./create-action-name");
    exports.createActionName = create_action_name_1.default;
    var configure_store_1 = require("./configure-store");
    exports.configureStore = configure_store_1.default;
    var create_action_1 = require("./create-action");
    exports.createAction = create_action_1.default;
    var create_async_action_1 = require("./create-async-action");
    exports.createAsyncAction = create_async_action_1.default;
    var create_sync_action_1 = require("./create-sync-action");
    exports.createSyncAction = create_sync_action_1.default;
    var create_effect_1 = require("./create-effect");
    exports.createEffect = create_effect_1.default;
    var create_fetch_effect_1 = require("./create-fetch-effect");
    exports.createFetchEffect = create_fetch_effect_1.default;
    var create_reducer_1 = require("./create-reducer");
    exports.createReducer = create_reducer_1.default;
    var create_saga_worker_1 = require("./create-saga-worker");
    exports.createSagaWorker = create_saga_worker_1.default;
    var create_state_1 = require("./create-state");
    exports.createState = create_state_1.default;
    var create_tasks_1 = require("./create-tasks");
    exports.createTasks = create_tasks_1.default;
    var create_watcher_1 = require("./create-watcher");
    exports.createWatcher = create_watcher_1.default;
    var create_data_flow_1 = require("./create-data-flow");
    exports.createDataFlow = create_data_flow_1.default;
    var merge_data_flow_1 = require("./merge-data-flow");
    exports.mergeDataFlow = merge_data_flow_1.default;
});
//# sourceMappingURL=index.js.map