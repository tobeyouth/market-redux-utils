(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "market-utils"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var market_utils_1 = require("market-utils");
    var createName = function (scope, action, ext) {
        return market_utils_1.camelCase([scope, action, ext].filter(function (item) {
            return !!item;
        }).join('-'));
    };
    exports.default = createName;
});
//# sourceMappingURL=create-name.js.map