var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var createReducer = function (scope, actions, state, reducers) {
        var _a;
        var STATE = __assign({}, state);
        var _reducer = function (state, action) {
            if (state === void 0) { state = STATE; }
            if (!action) {
                return state;
            }
            var type = action.type;
            if (reducers && reducers[type]) {
                return reducers[type](state, action);
            }
            return state;
        };
        return _a = {},
            _a[scope + "State"] = _reducer,
            _a;
    };
    exports.default = createReducer;
});
//# sourceMappingURL=create-reducer.js.map