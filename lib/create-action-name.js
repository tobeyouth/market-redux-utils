(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var createActionName = function (scope, action, status) {
        return [scope, action, status].filter(function (item) {
            return !!item;
        }).map(function (name) {
            return name.toUpperCase();
        }).join('.');
    };
    exports.default = createActionName;
});
//# sourceMappingURL=create-action-name.js.map