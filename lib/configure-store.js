var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "redux", "redux-saga", "redux-logger"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var redux_1 = require("redux");
    var redux_saga_1 = __importDefault(require("redux-saga"));
    var redux_logger_1 = __importDefault(require("redux-logger"));
    var creator;
    var sagaMiddleware = redux_saga_1.default();
    if (process && process.env && process.env.NODE_ENV !== 'dev') {
        creator = redux_1.compose(redux_1.applyMiddleware(sagaMiddleware))(redux_1.createStore);
    }
    else {
        creator = redux_1.compose(redux_1.applyMiddleware(sagaMiddleware), redux_1.applyMiddleware(redux_logger_1.default))(redux_1.createStore);
    }
    var configureStore = function (reducer, forkedSaga, initState) {
        var store = creator(reducer, initState);
        if (forkedSaga) {
            sagaMiddleware.run(forkedSaga);
        }
        return store;
    };
    exports.default = configureStore;
});
//# sourceMappingURL=configure-store.js.map