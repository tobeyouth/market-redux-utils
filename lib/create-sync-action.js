(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var createSyncAction = function (scope, action) {
        var _action = function (payload) {
            return {
                type: [scope, action].filter(function (k) {
                    return !!k;
                }).join('.').toUpperCase(),
                payload: payload
            };
        };
        _action.__async__ = false;
        _action.__scope__ = scope;
        _action.__actionName__ = action;
        return _action;
    };
    exports.default = createSyncAction;
});
//# sourceMappingURL=create-sync-action.js.map