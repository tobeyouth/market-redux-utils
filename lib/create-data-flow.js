var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./create-actions", "./create-effects", "./create-saga-workers", "./create-tasks", "./create-watcher", "./create-state", "./create-reducer", "redux", "redux-saga/effects", "market-utils", "./configure-store", "./create-name", "./helpers/reducer"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var create_actions_1 = __importDefault(require("./create-actions"));
    var create_effects_1 = __importDefault(require("./create-effects"));
    var create_saga_workers_1 = __importDefault(require("./create-saga-workers"));
    var create_tasks_1 = __importDefault(require("./create-tasks"));
    var create_watcher_1 = __importDefault(require("./create-watcher"));
    var create_state_1 = __importDefault(require("./create-state"));
    var create_reducer_1 = __importDefault(require("./create-reducer"));
    var redux_1 = require("redux");
    var effects_1 = require("redux-saga/effects");
    var market_utils_1 = require("market-utils");
    var configure_store_1 = __importDefault(require("./configure-store"));
    var create_name_1 = __importDefault(require("./create-name"));
    var reducer_1 = require("./helpers/reducer");
    var DataFlowSuppotTypes;
    (function (DataFlowSuppotTypes) {
        DataFlowSuppotTypes["list"] = "list";
        DataFlowSuppotTypes["detail"] = "detail";
    })(DataFlowSuppotTypes = exports.DataFlowSuppotTypes || (exports.DataFlowSuppotTypes = {}));
    exports.DEFAULT_DATAFLOW_OPTIONS = {
        type: DataFlowSuppotTypes.list,
        autoTasks: true,
        autoStore: true
    };
    var DataFlow = (function () {
        function DataFlow(scope, actionNames, options) {
            if (options === void 0) { options = exports.DEFAULT_DATAFLOW_OPTIONS; }
            this.__scope__ = scope;
            this.__actionNames__ = actionNames;
            this.__options__ = options;
            this.actions = this.createActions();
            this.effects = this.createEffects();
            this.sagaWorkers = this.createSagaWorkers();
            this.state = this.createState();
            this.watchers = this.createWatchers();
            this.reducer = this.createReducer();
            var autoTasks = options.autoTasks, autoStore = options.autoStore;
            if (autoTasks) {
                this.tasks = this.createTasks();
            }
            if (autoStore) {
                this.store = this.createStore();
            }
            if (this.store) {
                this.dispatch = this.store.dispatch;
            }
            if (this.store) {
                this.actions = redux_1.bindActionCreators(this.actions, this.dispatch);
            }
        }
        DataFlow.prototype.createActions = function () {
            return create_actions_1.default(this.__scope__, this.__actionNames__);
        };
        DataFlow.prototype.createEffects = function () {
            var options = this.__options__;
            var params = options ? {
                api: options.api
            } : {};
            var effects = create_effects_1.default(this.__scope__, this.__actionNames__, params);
            if (options && options.effects) {
                for (var name_1 in options.effects) {
                    effects[name_1] = options.effects[name_1];
                }
            }
            return effects;
        };
        DataFlow.prototype.createSagaWorkers = function () {
            if (!this.actions) {
                market_utils_1.errorLog('createSagaWorkers', 'There is no actions in DataFlow instance', 'create-data-flow');
                return null;
            }
            if (!this.effects) {
                market_utils_1.errorLog('createSagaWorkers', 'There is no effects in DataFlow instance', 'create-data-flow');
                return null;
            }
            var resCheck = this.__options__.resCheck;
            return create_saga_workers_1.default(this.actions, this.effects, resCheck);
        };
        DataFlow.prototype.createWatchers = function (effectFn) {
            var _this = this;
            if (effectFn === void 0) { effectFn = effects_1.takeLatest; }
            if (!this.sagaWorkers) {
                market_utils_1.errorLog('createWatchers', 'There is no sagaWorkers in DataFlow instance', 'create-data-flow');
                return null;
            }
            return Object.keys(this.sagaWorkers).map(function (sagaName) {
                var actionName = sagaName.replace(/SagaWorker$/gi, '');
                var action = _this.actions[actionName];
                var watchAction = action[action.__actionName__];
                var sagaWorker = _this.sagaWorkers[sagaName];
                return create_watcher_1.default(watchAction, sagaWorker, effectFn);
            });
        };
        DataFlow.prototype.createState = function () {
            var state = this.__options__ ? this.__options__.state : {};
            var stateName = create_name_1.default(this.__scope__, 'state');
            return create_state_1.default(this.__scope__, this.actions, state);
        };
        DataFlow.prototype.createReducer = function () {
            var options = this.__options__;
            var type = options && options.type ? options.type : 'list';
            var reducers = options ? options.reducers : {};
            var _reducers = Object.assign(reducer_1.createCommonReducer(type, this.actions), reducers);
            return create_reducer_1.default(this.__scope__, this.actions, this.state, _reducers);
        };
        DataFlow.prototype.createTasks = function () {
            if (!this.watchers) {
                market_utils_1.errorLog('createTasks', 'There is no watchers in DataFlow instance', 'create-data-flow');
                return null;
            }
            return create_tasks_1.default(this.watchers);
        };
        DataFlow.prototype.createStore = function () {
            if (!this.watchers) {
                market_utils_1.errorLog('createStore', 'There is no watchers in DataFlow instance', 'create-data-flow');
                return null;
            }
            var reducer = redux_1.combineReducers(this.reducer);
            return configure_store_1.default(reducer, this.tasks);
        };
        return DataFlow;
    }());
    exports.DataFlow = DataFlow;
    var createDataFlow = function (scope, actionNames, options) {
        var df = new DataFlow(scope, actionNames, options);
        return df;
    };
    exports.default = createDataFlow;
});
//# sourceMappingURL=create-data-flow.js.map