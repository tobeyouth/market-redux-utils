(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "market-utils"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var market_utils_1 = require("market-utils");
    var createAsyncAction = function (scope, actionName, proxy) {
        var asyncTypes = market_utils_1.createFetchTypes(scope, actionName);
        var _action = function (payload) {
            var _payload = payload;
            if (proxy) {
                _payload = proxy.call(this, payload);
            }
            return {
                type: asyncTypes[actionName],
                payload: _payload
            };
        };
        Object.keys(asyncTypes).forEach(function (_type) {
            _action[_type] = asyncTypes[_type];
        });
        _action.__async__ = true;
        _action.__scope__ = scope;
        _action.__actionName__ = actionName;
        return _action;
    };
    exports.default = createAsyncAction;
});
//# sourceMappingURL=create-async-action.js.map