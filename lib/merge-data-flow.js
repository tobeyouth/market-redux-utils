var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "redux", "./create-tasks", "./configure-store"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var redux_1 = require("redux");
    var create_tasks_1 = __importDefault(require("./create-tasks"));
    var configure_store_1 = __importDefault(require("./configure-store"));
    ;
    var MultipleDataFlow = (function () {
        function MultipleDataFlow(instances) {
            var scopes = instances.map(function (df) {
                return df.__scope__;
            });
            this.actions = instances.reduce(function (r, df) {
                var _actions = df.actions;
                var _a = checkSameKey(Object.keys(r), Object.keys(_actions)), hasSameKey = _a[0], key = _a[1];
                if (hasSameKey) {
                    console.warn("There is same action key: " + key + ", in instances");
                }
                return Object.assign(r, __assign({}, _actions));
            }, {});
            this.effects = instances.reduce(function (r, df) {
                var _effects = df.effects;
                var _a = checkSameKey(Object.keys(r), Object.keys(_effects)), hasSameKey = _a[0], key = _a[1];
                if (hasSameKey) {
                    console.warn("There is same effect key: " + key + ", in instances");
                }
                return Object.assign(r, __assign({}, _effects));
            });
            this.state = instances.reduce(function (r, df) {
                var _a;
                var _state = df.state;
                return Object.assign(r, (_a = {}, _a[_state.scope + "State"] = _state, _a));
            }, {});
            var reducer = instances.reduce(function (r, df) {
                var _reducer = df.reducer;
                var _a = checkSameKey(Object.keys(r), Object.keys(_reducer)), hasSameKey = _a[0], key = _a[1];
                if (hasSameKey) {
                    console.warn("There is same reducer key: " + key + ", in instances");
                }
                return Object.assign(r, _reducer);
            }, {});
            this.reducer = reducer;
            this.sagaWorkers = instances.reduce(function (r, df) {
                var _sagaWorkers = df.sagaWorkers;
                var _a = checkSameKey(Object.keys(r), Object.keys(_sagaWorkers)), hasSameKey = _a[0], key = _a[1];
                if (hasSameKey) {
                    console.warn("There is same sagaWorker key: " + key + ", in instances");
                }
                return Object.assign(r, __assign({}, _sagaWorkers));
            });
            this.watchers = instances.reduce(function (r, df) {
                var _watchers = df.watchers;
                return r.concat(_watchers);
            }, []);
            this.tasks = create_tasks_1.default(this.watchers);
            this.store = configure_store_1.default(redux_1.combineReducers(this.reducer), this.tasks);
            this.dispatch = this.store.dispatch;
            this.actions = redux_1.bindActionCreators(this.actions, this.dispatch);
        }
        return MultipleDataFlow;
    }());
    function checkSameKey(a, b) {
        var sameKey = a.find(function (item) {
            return b.find(function (_item) {
                return _item === item;
            });
        });
        return [!!sameKey, sameKey];
    }
    var mergeDataFlow = function () {
        var instances = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            instances[_i] = arguments[_i];
        }
        return new MultipleDataFlow(instances);
    };
    exports.default = mergeDataFlow;
});
//# sourceMappingURL=merge-data-flow.js.map