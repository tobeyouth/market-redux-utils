var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "market-utils", "./create-action"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var market_utils_1 = require("market-utils");
    var create_action_1 = __importDefault(require("./create-action"));
    var createActions = function (scope, actionNames) {
        return actionNames.reduce(function (r, actionName) {
            var _a;
            return Object.assign(r, (_a = {},
                _a[market_utils_1.camelCase(scope + "-" + actionName)] = create_action_1.default(scope, actionName),
                _a));
        }, {});
    };
    exports.default = createActions;
});
//# sourceMappingURL=create-actions.js.map