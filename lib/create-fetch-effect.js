(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "market-utils"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var market_utils_1 = require("market-utils");
    var createFetchEffect = function (method, api) {
        if (method === void 0) { method = 'get'; }
        return function (params) {
            return market_utils_1.Fetch[method](api, params).then(function (res) {
                return res;
            }).catch(function (err) {
                return err;
            });
        };
    };
    exports.default = createFetchEffect;
});
//# sourceMappingURL=create-fetch-effect.js.map