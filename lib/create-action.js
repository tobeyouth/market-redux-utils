var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "market-utils", "./create-async-action", "./create-sync-action"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var market_utils_1 = require("market-utils");
    var create_async_action_1 = __importDefault(require("./create-async-action"));
    var create_sync_action_1 = __importDefault(require("./create-sync-action"));
    exports.default = (function (scope, action, proxy) {
        if (market_utils_1.getType(proxy) === 'boolean') {
            return create_sync_action_1.default(scope, action);
        }
        return create_async_action_1.default(scope, action, proxy);
    });
});
//# sourceMappingURL=create-action.js.map